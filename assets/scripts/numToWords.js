let singleDigit = ["", "One", "Two", "Three", "Four", "Five","Six","Seven","Eight","Nine"];
let tens = ["", "Ten","Twenty","Thirty","Fourty","Fifty","Sixty","Seventy","Eighty","Ninety"];
let teens = ["", "Eleven","Tweleve","Thirteen", "Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"];

function numConverter(inputNo){
	if(inputNo === 0){
		return "Zero";
	}
	if(inputNo < 10 && inputNo >=0){
		return	singleDigit[inputNo];
	}
	if(inputNo < 99 && inputNo%10 === 0){
		return tens[inputNo/10];
	}
	if(inputNo < 20 && inputNo > 10){
		return teens[inputNo-10];
	}
	if(inputNo > 20 && inputNo < 100){
		let singleValue = inputNo % 10;
		let tensValue = (inputNo-singleValue)/10;
		return tens[tensValue]+" "+singleDigit[singleValue];
	}

	//FOR HUDRETHS
	if(inputNo >= 100 && inputNo<1000){
		let singleValue = (inputNo-(inputNo%100))/100;
		let tensValue = ((inputNo%100)-((inputNo%100)%10))/10;
		let hundredValue = (inputNo%100)%10;
		if(tensValue>1){
			return singleDigit[singleValue]+ " Hundred " + tens[tensValue] +" "+ singleDigit[hundredValue];
		}else if(tensValue === 0){
			return singleDigit[singleValue]+ " Hundred " + singleDigit[hundredValue] ;
		}else{
			if(hundredValue !== 0)
			{
				return singleDigit[singleValue]+ " Hundred " + teens[hundredValue] ;
			}else{
				return singleDigit[singleValue]+ " Hundred " + "Ten";
			}
		}
	}

	//FOR THOUSANDTHS
	if(inputNo >=1000 && inputNo<10000){
		let singleValue = (inputNo-(inputNo%1000))/1000;
		let	tensValue =   ((inputNo%1000)-(inputNo%1000)%100)/100;
		let	hundredValue =  (((inputNo%1000)%100)-((inputNo%1000)%100)%10)/10;
		let thousandValue = (((inputNo%1000)%100)%10);
		if (tensValue>=1){ 
			if(hundredValue > 1){ 
				return singleDigit[singleValue]+ " Thousand "+ singleDigit[tensValue] + " Hundred " +  tens[hundredValue] +" "+ singleDigit[thousandValue];
			}else if(hundredValue===1){ 
				if(hundredValue===1 && thousandValue==0){ 
					return singleDigit[singleValue]+ " Thousand "+ singleDigit[tensValue] + " Hundred " +  "Ten";
				}else{
					return singleDigit[singleValue]+ " Thousand "+ singleDigit[tensValue] + " Hundred " +  teens[thousandValue];
				}
			}else{
				return singleDigit[singleValue]+ " Thousand "+ singleDigit[tensValue] + " Hundred " +  singleDigit[thousandValue];
			}
		}
		else{
			if(hundredValue > 1){
				return singleDigit[singleValue]+ " Thousand "+ singleDigit[tensValue] + tens[hundredValue] +" "+ singleDigit[thousandValue];
			}else if(hundredValue===1){
				if(hundredValue===1 && thousandValue==0){
					return singleDigit[singleValue]+ " Thousand "+ singleDigit[tensValue] + "Ten";
				}else{
					return singleDigit[singleValue]+ " Thousand "+ singleDigit[tensValue] + teens[thousandValue];
				}
			}else{
				return singleDigit[singleValue]+ " Thousand "+ singleDigit[tensValue] + singleDigit[thousandValue];
			}
		}
	}
}