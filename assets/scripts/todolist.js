let allTasks = [];

function showTasks(){
	allTasks.forEach(function(task, index){
	console.log("Task #"+(index+1)+": "+task)
	})
}

function addTask(newTask){
	allTasks.push(newTask);
}
function deleteTask(taskNo){
	let realIndex = taskNo - 1;
	allTasks.splice(realIndex,1);
}
function editTask(taskNo, editedTask){
	let realIndex = taskNo - 1;
	allTasks.splice(realIndex,1, editedTask);
}

addTask("Eat");
addTask("Sleep");
addTask("Sleep More");
showTasks();
deleteTask(0);
showTasks();
editTask(1,"Drink");
showTasks();



